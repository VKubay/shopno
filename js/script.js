$(document).ready(function () {


    $('.about-us-slider').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 2500
    });


    /*---------- Drop Down Navigation --------*/


    $(document).on('click', '.dropdown-toggle', function () {
        $(this).next('.dropdown').slideToggle();
        $('#burger').toggleClass('active-sandwich');
        return false;
    });


    $(document).on('click',function (e) {
        if (jQuery('.dropdown').css('display') === 'block') {
            jQuery('.dropdown').slideToggle();
            $('#burger').toggleClass('active-sandwich');
        }
    });

    $(document).on('click', 'a[href*="#"]:not([href="#"])',  function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                jQuery('.dropdown').slideToggle();
                $('#burger').toggleClass('active-sandwich');
                return false;
            }
        }
    });

    /*---------- End of Drop Down Navigation --------*/


    $('.team-item').hover(function () {
        $(this).find('.team-item-description').toggleClass('active-team')
    });


    $('.slider-xl').slick({
        autoplay: false,
        arrows: true

    });

});

